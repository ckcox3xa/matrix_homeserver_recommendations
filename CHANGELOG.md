## 0.2.0

- Update joinMatrix.org parser
- Remove dependencies

## 0.1.4

- Fix negative durations

## 0.1.3

- Bugfix of empty strings

## 0.1.2

- Fix error in joinMatrix.org parser

## 0.1.1

- Implement MatrixApi filter

## 0.1.0

- Initial version.
